# Konux API Acceptance Tests
This test framework is for creating and running the acceptance test suite for RestCountries API

## How to Run Locally ?

#### Pre Requisite for creating new tests to the suite
JAVA 11 should be installed in your machine.

1. Rename `.env.example` file to `.env`
2. Update the `.env` file with the right URL. Currently it's set as `production` URL
4. Execute `./gradlew test` from root folder
5. The reports could be found inside `build/reports/tests/test` folder. Open `index.html` to see the report

## How to run in Docker ?
#### Pre Requisites
Docker should be installed in your machine

1. Build docker image. `docker build -t acceptance-api:1.0 -f docker/Dockerfile .`
2. Run docker image. `docker run -it --name acceptance acceptance-api:1.0`
3. To view the report. `docker cp acceptance:/acceptance-api-tests/build/reports .`
You can see the `reports` folder in your directory. Open `index.html`
4. Remove the docker container. `docker rm acceptance`   