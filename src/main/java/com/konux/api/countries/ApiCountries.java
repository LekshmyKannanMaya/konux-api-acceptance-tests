package com.konux.api.countries;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class ApiCountries {
	public Response getAllCountries(){
		return RestAssured
			.get("/all")
			.andReturn();
	}

	public Response getSingleCountry(String countryName){
		return RestAssured
			.get("/name/" + countryName)
			.andReturn();
	}

	public Response getCurrency(String currencyCode) {
		return RestAssured
			.get("/currency/" + currencyCode)
			.andReturn();
	}

	public Response getRegion(String regionName){
		return RestAssured
			.get("/region/" + regionName)
			.andReturn();
	}

	public Response getCountryByCapital(String capitalCity){
		return RestAssured
			.get("/capital/" + capitalCity)
			.andReturn();
	}
}
