package com.konux.hooks;

import io.github.cdimascio.dotenv.Dotenv;
import io.restassured.RestAssured;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class SetUp implements BeforeAllCallback {
	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		String baseURL;
		try {
			Dotenv dotenv = Dotenv.load();
			baseURL = dotenv.get("BASE_URL");
		}
		catch (Exception ex) {
			baseURL = System.getenv("BASE_URL");
		}
		if (baseURL == null) {
			throw new AssertionError("Please pass the BASE_URL either as environment variable or through .env file");
		}
		RestAssured.baseURI = baseURL;
	}
}
