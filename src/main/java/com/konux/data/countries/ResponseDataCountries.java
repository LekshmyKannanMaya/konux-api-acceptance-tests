package com.konux.data.countries;

public class ResponseDataCountries {
	private String name;
	private String[] topLevelDomain;
	private String alpha2Code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getTopLevelDomain() {
		return topLevelDomain;
	}

	public void setTopLevelDomain(String[] topLevelDomain) {
		this.topLevelDomain = topLevelDomain;
	}

	public String getAlpha2Code() {
		return alpha2Code;
	}

	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}
}
