package com.konux.apicountries;

import com.konux.api.countries.ApiCountries;
import com.konux.data.countries.ResponseDataCountries;
import com.konux.hooks.SetUp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SetUp.class)
public class SingleCountryApi {
	@Test
	@DisplayName("Test if the country details is listed for specific country")
	public void testRestCountryByCountryName() {
		ResponseDataCountries[] dataCountries = new ApiCountries()
			.getSingleCountry("Afghanistan")
			.then()
			.statusCode(200)
			.extract()
			.as(ResponseDataCountries[].class);
		Assertions.assertEquals("AF", dataCountries[0].getAlpha2Code());
	}
}
