package com.konux.apicountries;

import com.konux.api.countries.ApiCountries;
import com.konux.data.countries.ResponseDataCountries;
import com.konux.hooks.SetUp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SetUp.class)
public class AllCountriesRest {
	@Test
	@DisplayName("Test if all countries are listed")
	public void testAllCountries(){
		ResponseDataCountries [] dataCountries = new ApiCountries()
			.getAllCountries()
			.then()
			.statusCode(200)
			.extract()
			.as(ResponseDataCountries[].class);
		Assertions.assertTrue(dataCountries.length > 0);
	}

	@Test
	@DisplayName("Test if the country name is listed")
	public void testRestCountryName() {
		ResponseDataCountries [] dataCountries = new ApiCountries()
			.getAllCountries()
			.then()
			.statusCode(200)
			.extract()
			.as(ResponseDataCountries[].class);
		Assertions.assertEquals("Afghanistan", dataCountries[0].getName());
	}
}
