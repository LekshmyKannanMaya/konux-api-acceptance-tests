package com.konux.apicountries;

import com.konux.api.countries.ApiCountries;
import com.konux.data.countries.ResponseDataCountries;
import com.konux.hooks.SetUp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(SetUp.class)
public class GetCurrencyApi {
	@Test
	@DisplayName("Test if the country details is listed for specific currency")
	public void testRestCountryNameByCurrency() {
		ResponseDataCountries[] dataCountries = new ApiCountries()
			.getCurrency("AFN")
			.then()
			.statusCode(200)
			.extract()
			.as(ResponseDataCountries[].class);
		Assertions.assertEquals("Afghanistan", dataCountries[0].getName());
	}
}
